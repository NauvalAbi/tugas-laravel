<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(){
    return view('items.table');
    }

    public function datatables(){
        return view('items.datatables');
        }
}
