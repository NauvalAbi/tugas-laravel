<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
        public function create(){
            return view('pertanyaan.create');
            }

        public function store(Request $request){
            // dd($request->all())
            $request->validate([
                'judul' => 'required|unique:pertanyaan',
                'isi'   => 'required'
            ]);

            // $query = DB::table('pertanyaan')->insert([
            //     "judul" => $request["judul"],
            //     "isi"   => $request["isi"]
            // ]);

            // $pertanyaan = new Pertanyaan;
            // $pertanyaan->judul = $request["judul"];
            // $pertanyaan->isi = $request["isi"];
            // $pertanyaan->save();

            $pertanyaan = Pertanyaan::create([
                "judul" => $request["judul"],
                "isi" => $request["isi"]
            ]);

            return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Disimpan !');
        }

        public function index(){
            // $pertanyaan = DB::table('pertanyaan')->get();
            $pertanyaan = Pertanyaan::all();
            //dd($pertanyaan);
            return view('pertanyaan.index', compact('pertanyaan'));
        }

        public function show($pertanyaan_id){
            // $pertanyaan = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
            $pertanyaan = Pertanyaan::find($pertanyaan_id);
            // dd($pertanyaan);
            return view('pertanyaan.show', compact('pertanyaan'));
        }

        public function edit($pertanyaan_id){
            // $pertanyaan = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
            $pertanyaan = Pertanyaan::find($pertanyaan_id);
            // dd($pertanyaan);
            return view('pertanyaan.edit', compact('pertanyaan'));
        }

        public function update($pertanyaan_id, Request $request){
            $request->validate([
                'judul' => 'required',
                'isi'   => 'required'
            ]);

            // $query = DB::table('pertanyaan')
            //             ->where('id', $pertanyaan_id)
            //             ->update([
            //                 'judul' => $request['judul'],
            //                 'isi' => $request['isi']
            //             ]);

            $pertanyaan = Pertanyaan::where('id', $pertanyaan_id)->update([
                "judul" => $request["judul"],
                "isi" => $request["isi"]
            ]);

            return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Diubah !');
        }

        public function destroy($pertanyaan_id){
            // $query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->delete();
            Pertanyaan::destroy($pertanyaan_id);
            
            return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil di Hapus !');
        }
}
