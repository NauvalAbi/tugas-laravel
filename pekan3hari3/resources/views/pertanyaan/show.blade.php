@extends('template.master')

@section('content')
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Detail Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Judul Pertanyaan</label>
                    <p> {{ $pertanyaan->judul }} </p>

                    <div class="mt-3 mb-3"></div>
                    <label for="judul">Isi Pertanyaan</label>
                    <p> {{ $pertanyaan->isi }} </p>
                  </div>
                
                <!-- /.card-body -->
            </div>


@endsection