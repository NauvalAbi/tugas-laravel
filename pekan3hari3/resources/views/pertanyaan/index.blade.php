@extends('template.master')

@section('content')
@if(session('success'))
  <div class="alert alert-success">
  {{ session('success') }}
  </div>
@endif

<a class="btn btn-primary mb-3" href="{{ route ('pertanyaan.create') }}">Buat Pertanyaan</a>
<table class="table table-bordered">
  <thead>                  
    <tr>
      <th style="width: 10px">No. </th>
      <th>Judul Pertanyaan</th>
      <th>Isi Pertanyaan</th>
      <th style="width: 40px"><center>Aksi</center></th>
    </tr>
  </thead>
  <tbody>
  @forelse($pertanyaan as $key => $tanya)
    <tr>
      <td> {{ $key + 1 }} </td>
      <td> {{ $tanya->judul }} </td>
      <td> {{ $tanya->isi }} </td>
      <td style="display:flex">
      <a class="btn btn-success btn-sm mr-1" href="/pertanyaan/{{$tanya->id}}">Detail</a>
      <a class="btn btn-warning btn-sm" href="/pertanyaan/{{$tanya->id}}/edit">Edit</a>

      <form action="/pertanyaan/{{$tanya->id}}" method="POST">
      @csrf
      @method('DELETE')
      <input type="submit" value="delete" class="btn btn-danger btn-sm ml-1">
      </form>
      </td>
    </tr>
    @empty
    <tr>
      <td colspan="4" align="center"> Tidak Ada Pertanyaan</td>
    </tr>
    @endforelse
  </tbody>
</table>
@endsection