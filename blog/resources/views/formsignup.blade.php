<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Buat Akun</title>
</head>

<body>
    <!-- Judul -->
    <div>
        <h1>Buat Account Baru!</h1>
    </div>

    <!-- Form Sign Up -->
    <div>
        <h3>Sign Up Form</h3>
        <form method="POST" action="/welcome">
            @csrf
            <p>First Name :</p>
            <input type="text" name="nama1" id="firts_name">

            <p>Last Name :</p>
            <input type="text" name="nama2" id="last_name">

            <p>Gender : </p>
            <input type="radio" value="1">Laki - Laki<br>
            <input type="radio" value="2">Perempuan<br>
            <input type="radio" value="3">Other

            <p>Nationality : </p>
            <select>
                <option value="1">Indonesia</option>
                <option value="2">Singapore</option>
                <option value="3">Malaysia</option>
                <option value="4">India</option>
                <option value="5">China</option>
            </select>

            <p>Language Spoken : </p>
            <input type="checkbox" value="1"> Bahasa Indonesia <br>
            <input type="checkbox" value="2"> English <br>
            <input type="checkbox" value="3"> Other

            <p>Bio : </p>
            <textarea cols="40" rows="10"></textarea>
            <br><br>

            <input type="submit" value="Sign Up">
        </form>
    </div>
    
</body>
</html>