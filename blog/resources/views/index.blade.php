<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SanberBook</title>
</head>

<body>
    <!-- Judul -->
    <div>
        <h1>SanberBook</h1>
        <h2>Social Media Developer Santai Berkualitas</h2>
        <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>
    </div>

    <!-- Benefit -->
    <div>
        <h2>Benefit Join di SanberBook</h2>
        <ul>
            <li>Motivasi dar sesama Developer</li>
            <li> Sharing knowledge dari para mastah Sanber</li>
            <li>Dibuat oleh calon developer</li>
        </ul>
    </div>

    <!-- Cara Bergabung -->
    <div>
        <h2>Cara Bergabung SanberBook</h2>
        <ol>
            <li>Mengunjungi Website ini</li>
            <li>Mendaftar di <a href="/register" target="_blank"> Form SignUp</a></li>
            <li>Selesai!</li>
        </ol>
    </div>
 
</body>
</html>